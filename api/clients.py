import os

import pymongo
import pyrebase
import requests
from cachecontrol import CacheControl
from jose import jwt
from minio import Minio
from minio.error import ResponseError

# Connect to mongodb and select database
if os.environ.get("MONGODB_URI"):
    credentials = os.environ.get("MONGODB_URI")
else:
    with open('dbcredentials.txt') as f:
        credentials = f.readline().strip()

mclient = pymongo.MongoClient(credentials)
toshokan = mclient['toshokan']

minio_config = toshokan.settings.find_one({"_id":"minio"})
objstore = Minio(minio_config["url"],
                  access_key=minio_config["access_key"],
                  secret_key=minio_config["secret_key"],
                  secure=minio_config["secure"])

firebase_config = toshokan.settings.find_one({"_id":"firebase"})
del firebase_config["_id"]
firebase = pyrebase.initialize_app(firebase_config)
auth = firebase.auth()

class JWT_Verifier():
    cert_url = "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com"
    def __init__(self):
        session = requests.session()
        self.session = CacheControl(session)
        self.session.get(JWT_Verifier.cert_url)
        return
    def verify_token(self, token):
        r = self.session.get(JWT_Verifier.cert_url)
        certificates = r.json()
        try:
            decoded_token = jwt.decode(token, certificates, algorithms="RS256", audience=firebase_config["projectId"])
        except:
            raise ValueError
        return decoded_token
