from datetime import datetime

from bson.dbref import DBRef
from pymongo import UpdateOne

from api.clients import toshokan, auth

def get_episodes(user, type, id):
    return list(toshokan.list_episodes.find({"user":user, "library_ref": DBRef(type, id)}, {'_id': 0, 'library_ref': 0, 'date_added': 0, 'last_modified': 0, 'user': 0}))

def set_episodes(user, type, id, episodes):
    if not episodes:
        return None

    if type != 'anime' and type != 'manga':
        return {"msg": "Per-episode tracking is only supported for anime and manga"}, 400

    obj = toshokan[type].find_one({"_id": id}, {"_id":1, "episode_count":1, "chapter_count":1, "volume_count":1})
    if not obj:
        return {"msg":"Object of type %s and _id %s not found in library." % (type, id)}, 400

    for ep in episodes:
        if (type == 'anime' and ep['number'] > obj['episode_count'] and obj['episode_count'] is not None) or\
           (type == 'manga' and ep['number']> obj['chapter_count'] and obj['chapter_count'] is not None):
            return {"msg": "Episode number %s is too big." % (ep['number'])}, 400

    ops = [UpdateOne({"user": user,
                      "library_ref": DBRef(type, id),
                      "number": episode['number']},
                     {"$set": {"last_modified": datetime.utcnow(),
                               "comments": episode['comments'],
                               "score": episode['score'],
                               "progress": episode['progress']},
                      "$setOnInsert": {"date_added": datetime.utcnow()}},
                     upsert=True)
           for episode in episodes]

    toshokan.list_episodes.bulk_write(ops)

def get_user_list(user, type):
    episodes = list(toshokan.list_episodes.find({"user": user},{'_id': 0, 'date_added': 0, 'last_modified': 0, 'user': 0}))
    lst = list(toshokan.lists.find({"user":user, "library_ref.$ref":type}, { "_id":0, "user":0}))
    user_list = {entry['library_ref'].id: {**entry, 'episodes':[]} for entry in lst}
    min_data = toshokan[type].find({"_id":{"$in": [entry["library_ref"].id for entry in user_list.values()] }}, {"_id":1,"title":1, "episode_count":1, "type":1, "duration":1, "volume_count":1, "chapter_count":1, "length": 1})

    for ep in episodes:
        x = user_list[ep['library_ref'].id]
        user_list[ep['library_ref'].id]['episodes'].append({k:v for k, v in ep.items() if k != 'library_ref'})

    return {
            "list": [
                {
                    '_id': entry['library_ref'].id,
                    'date_added': entry['date_added'],
                    'last_modified': entry['last_modified'],
                    'entry': {key: value for key, value in entry.items() if key not in ['library_ref', 'date_added', 'last_modified']}}
                    for entry in user_list.values()
            ],
            "data": {
                        obj["_id"]: { key: value for key, value in obj.items() if key != "_id"} 
                        for obj in min_data
                    }
            }

def get_list_entry(user, type, id):
    entry = toshokan.lists.find_one({"user": user, "library_ref": DBRef(type, id)}, {"_id":0, "library_ref":0, "user":0, "date_added":0, "last_modified":0})
    entry['episodes'] = get_episodes(user, type, id)
    return entry or (None, 204)

def set_list_entry(user, type, id, entry):
    obj = toshokan[type].find_one({"_id": id}, {"_id":1, "episode_count":1, "chapter_count":1, "volume_count":1})
    if not obj:
        return {"msg":"Object of type %s and _id %s not found in library." % (type, id)}, 400

    if type == "anime":
        if entry.get("volumes"):
            del entry["volumes"]

    try: # make sure progress isn't higher than complete
        if obj.get("episode_count") and obj["episode_count"] < entry["progress"]:
            raise ValueError
        if obj.get("chapter_count") and obj["chapter_count"] < entry["progress"]:
            raise ValueError
        if obj.get("volume_count") and entry.get("volumes") and obj["volume_count"] < entry.get("volumes", 0):
            raise ValueError
    except ValueError:
        return {"msg":"'progress' and/or 'volumes' value invalid."}, 400

    try:
        episodes = entry['episodes']
        del entry['episodes']
    except KeyError:
        episodes = []

    ep_return = set_episodes(user, type, id, episodes)
    if ep_return is not None: return ep_return

    toshokan.lists.update_one({"user": user, "library_ref": DBRef(type, id)},
                              {"$set": {**entry, "last_modified": datetime.utcnow()},
                              "$setOnInsert":{"date_added": datetime.utcnow()}},
                              upsert=True)
    return {"msg":"Updated entry."}

def delete_list_entry(user, type, id):
    toshokan.lists.delete_one({"user": user, "library_ref": DBRef(type, id)})
    return {"msg":"Deleted entry."}

def replace_user_list(user, type, list_object, origin=None):
    # FIXME (?) this currently does not take per-episode information as a parameter, but that's not important since we only use this for importing from sources that do not have it anyways
    entry_list = list_object["list"]
    id_set = { entry["_id"] for entry in entry_list}

    if not origin: # verify that the anime corresponding to these ids exist on the site
        existing_ids = toshokan[type].find( { "_id": { "$in": list(id_set) } }, { "_id":1 }).count();
        if not (existing_ids == len(id_set)):
            return {"msg":"Object of type %s not found in library." % (type)}, 400

    else: # translate ids from MAL IDs, anilist etc into Toshokan IDs
        id_set = {int(el) for el in id_set} # MAL IDs are integers.
        toshokan_library_ids = toshokan[type].find( { "external_links.%s" % origin: { "$in": list(id_set) } }, { "_id":1, "external_links.%s" % origin:1 })
        id_translation = {element["external_links"][origin]: element["_id"] for element in toshokan_library_ids}
        for e in entry_list:
            if e["entry"]["score"] == 0:
                e["entry"]["score"] = None
        entry_list = [{"_id": id_translation.get(int(entry["_id"])), "entry": entry["entry"]} for entry in entry_list if id_translation.get(int(entry["_id"]), False)]

    toshokan.lists.delete_many({"user":user, "library_ref.$ref":type})
    ops = [UpdateOne({"user": user, "library_ref": DBRef(type, entry["_id"])},
                      {"$set": {**entry["entry"], "last_modified": datetime.utcnow()},
                      "$setOnInsert":{"date_added": datetime.utcnow()}},
                      upsert=True)
          for entry in entry_list]

    if ops:
        toshokan.lists.bulk_write(ops)

    return { "msg":"Updated list." }

def get_public_list(username, type):
    list_user = toshokan["users"].find_one({"username":username})
    if not list_user:
        return {}, 404
    if list_user["settings"]["list_public"]:
        return get_user_list(list_user["_id"], type)
    else:
        return {"msg":"list_private"}, 403
