import os
from datetime import datetime
from api.vndb_handler import VNDBError, VNDBThrottled, VNDBSocket

def get_vn_list(username):
    if os.environ.get("VNDB_USERNAME") and os.environ.get("VNDB_PASSWORD"):
        account = os.environ.get("MONGODB_URI"). os.environ.get("VNDB_PASSWORD")
    else:
        with open('vndb_details.txt') as f:
            account = f.readline().strip().split(':')

    statuses = {
        0: 'planned',
        1: 'current',
        2: 'completed',
        3: 'on-hold',
        4: 'dropped'
    }
    try:
        soc = VNDBSocket('api.vndb.org', 19534, account[0], account[1])
        uid = soc.get('username="{}"'.format(username), dtype='user')['items'][0]['id']
    except (IndexError, VNDBError):
        return []

    more = True
    vnid = 0
    vnlist = []
    scores = {}

    while more:
        data = soc.get('(uid={}) and (vn>={})'.format(uid, vnid+1), options={"results":100}, dtype='votelist')
        more = data['more']
        for vn in data['items']:
            scores[vn['vn']] = vn['vote'] / 10.0
            vnid = max(vnid, vn['vn'])

    vnid = 0
    more = True

    while more:
        data = soc.get('(uid={}) and (vn>={})'.format(uid, vnid+1), options={"results":100}, dtype='vnlist')
        more = data['more']
        for vn in data['items']:
            vnlist.append(
                {
                    '_id': str(vn['vn']),
                    'entry':
                    {
                        '_id': vn['vn'],
                        'score': scores.get(vn['vn']) or None,
                        'status': statuses[vn['status']],
                        'comments': vn['notes'] or '',
                        'volumes': 0,
                        'progress': 0,
                        'repetitions': 0,
                        'start_date': datetime.fromtimestamp(vn['added']).isoformat(),
                        'end_date': None
                    }
                })
            vnid = max(vnid, vn['vn'])

    return vnlist
