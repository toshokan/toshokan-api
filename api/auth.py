import time
import datetime
from collections import defaultdict

from minio import PostPolicy

from api.clients import toshokan, auth, objstore, JWT_Verifier
from api.library import min_projection, creature_min_projection

verifier = JWT_Verifier()

# Firebase token & account validation, called on every request that requires auth
def token_info(access_token):
    try:
        # Check if token is valid and decode, will throw exception otherwise.what abo
        user_data = verifier.verify_token(access_token)

        uid = user_data["user_id"]
        if not toshokan.users.find_one({"_id":uid}, {"_id":1}):
            raise ValueError # User is not in mongodb

        return {'uid': uid, 'scope': ['user']}
    except:
        pass
    return None

#X-Accel redirect !important
def x_accel(user, last_event_id = ""):
    return {"msg": "you're in."}, 200, {"X-Accel-Redirect": "/sub/internal/%s?last_event_id=%s" % (user, last_event_id), "X-Accel-Buffering":"no"}

# API functions
def complete_registration(user_data): # called after registration. Adds the user to our local DB.
    access_token = user_data["token"]
    try:
        token_data = verifier.verify_token(access_token)
        user_id = token_data["user_id"]
    except:
        return {"msg":"Invalid Token."}, 401
    try:
        new_user = {"_id": user_id,
                    "username" : user_data["username"],
                    "settings": {
                        "list_public": True,
                        "profile_public": True,
                        "pagination": False
                    },
                    "profile": {
                        "favourites": {
                            "anime": [],
                            "manga": [],
                            "novel": [],
                            "vn": [],
                            "person": [],
                            "character": []
                        },
                        "avatar": "anime.jpg", # TODO default
                        "biography": "",
                        "birthday": None,
                        "location": None,
                        "gender": None
                    },
                    "date_registered": datetime.datetime.now()}
        toshokan.users.insert_one(new_user)
    except:
        if toshokan.users.find_one({"username":new_user["username"]}):
            return {"msg":"Username already taken."}, 400

        if toshokan.users.find_one({"_id":new_user["_id"]}):
            return {"msg":"User already completed registration."}, 400
        return {"msg":"Could not complete registration."}, 400

    return {"msg":"Registered User %s as %s." % (user_id, user_data["username"])}

def is_registered(user_data):
    access_token = user_data["token"]
    try:
        token_data = verifier.verify_token(access_token)
        uid = token_data["user_id"]
        if toshokan.users.find_one({"_id":uid}):
            return {"registered": True}, 200
        else:
            return {"registered": False}, 200
    except:
        pass
    return {"msg":"Invalid token."}, 401

def login(credentials):
    try:
        user = auth.sign_in_with_email_and_password(credentials["email"], credentials["password"])
    except:
        return {"msg":"Credentials not accepted."}, 401

    return {"token":user["idToken"], "refreshToken":user["refreshToken"]}

def refresh_token(refreshToken):
    try:
        user = auth.refresh(refreshToken['refreshToken'])
    except:
        return {"msg":"Invalid refresh token."}, 401
    return {"token":user["idToken"]}

# Account functions
def get_account_data(user):
    return toshokan.users.find_one({"_id":user}, {"list": 0, "following":0})

def get_account_settings(user):
    return toshokan.users.find_one({"_id":user}, {"settings": 1})

def set_account_settings(user, settings):
    toshokan.users.update_one({"_id":user}, {"$set": {"settings": settings}})
    return {"msg": "Success"}, 200

def get_user_profile(user):
    user_data = toshokan.users.find_one({"_id":user}, {"profile": 1,  "following":1, "username":1})
    profile = user_data.get("profile")
    if profile and profile.get("favourites"):
        data = {}
        for type, fav_list in profile["favourites"].items():
            if type in ["manga", "anime", "novel", "vn"]:
                proj = min_projection
            elif type in ["person", "character"]:
                proj = creature_min_projection
            else:
                continue
            objects = list(toshokan[type].find({'_id': {'$in': fav_list}}, proj))
            data[type] = {object["_id"]:object for object in objects}
            user_data["data"] = data
    following = user_data.get("following", [])
    following_valid = [user["username"] for user in toshokan.users.find({"_id": {"$in": following}})]
    user_data["following"] = following_valid
    return user_data

def set_user_profile(user, profile):
    if profile.get("favourites"): #verify existence
        for type, fav_list in profile["favourites"].items():
            if type in ["manga", "anime", "novel", "vn", "person", "character"]:
                objects = list(toshokan[type].find({'_id': {'$in': fav_list}}, {"_id":1}))
                if len(objects) < len(fav_list):
                    return {"msg": "Duplicate or non-existent favourites"}, 400

    toshokan.users.update_one({"_id":user}, {"$set": {"profile": profile}})
    return {"msg": "Success"}, 200

def get_public_profile(username):
    user = toshokan.users.find_one({"username":username}, {"profile": 1, "settings":1, "following":1})
    if not user["settings"]["profile_public"]:
        return {"msg": "profile is private"}, 403
    del user["settings"]

    profile = user.get("profile")
    if profile and profile.get("favourites"):
        data = {}
        for type, fav_list in profile["favourites"].items():
            if type in ["manga", "anime", "novel", "vn"]:
                proj = min_projection
            elif type in ["person", "character"]:
                proj = creature_min_projection
            else:
                continue
            objects = list(toshokan[type].find({'_id': {'$in': fav_list}}, proj))
            data[type] = {object["_id"]:object for object in objects}

        user["data"] = data
    following = user.get("following", [])
    following_valid = [user["username"] for user in toshokan.users.find({"_id": {"$in": following}})]
    user["following"] = following_valid
    return user

def get_hot():
    entries = list(toshokan.settings.find({'_id': 'hot'}, {'_id': 0}))[0]
    objs = defaultdict(dict)
    for type_ in entries:
        objs[type_] = list(toshokan[type_].find({'_id': {'$in': entries[type_]}}, min_projection))

    return dict(objs)

def avatar_upload(user):
    post_policy = PostPolicy()
    post_policy.set_bucket_name('users')
    post_policy.set_key_startswith('myobject')
    post_policy.set_content_type('image/jpeg')
    post_policy.set_content_length_range(16, 5242880)
    post_policy.set_key(user+'/avatar.jpg')
    expires_date = datetime.datetime.utcnow()+datetime.timedelta(minutes=30)
    post_policy.set_expires(expires_date)
    signed_policy = objstore.presigned_post_policy(post_policy)
    return { "url": signed_policy[0], "form_data": signed_policy[1] }, 200
