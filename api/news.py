import pymongo
from api.clients import toshokan

def get_index():
    entries = []
    news_posts = toshokan.news_posts.find({},{"contents":0}).sort("date", -1).limit(20)
    for post in news_posts:
        entries.append(
            {'blurb': post['blurb'],
             'date': post['date'],
             'href': '/news/posts/'+post['id'],
             'title': post['title']
            }
        )
    return {'data': {'posts': entries}}

def get_post(id):
    post = toshokan.news_posts.find_one({'id':id})
    return {'data':
            {'contents': post['contents'],
             'title': post['title'],
             'date': post['date']}}
