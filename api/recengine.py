import os
import math
import time
import datetime

from pymongo import UpdateOne
from bson.dbref import DBRef
import turicreate as tc

from numpy import dot
from numpy.linalg import norm

from api.clients import toshokan

class Recengine:
    def __init__(self, default_paths):
        self.models = {}
        self.model_paths = {}
        self.default_paths = default_paths

    def _ensure(f):
        # ensure model is loaded and up to date
        # requires the first argument to be the model name
        def wrapper(self, name, *args, **kwargs):
            self.ensure_model(name, self.model_paths.get(name))
            return f(self, name, *args, **kwargs)
        return wrapper

    def refresh(self):
        for name, path in model_paths.items():
            self.ensure_model(name, path)

    def ensure_model(self, name, path):
        if not self.model_paths.get(name): # If there's no path registered for this model
            self.model_paths[name] = self.default_paths.get(name) # Use a default path

        if not path:
            path = self.model_paths[name]

        try:
            if (not self.models.get(name) # New model
                or self.model_paths.get(name) != path # Existing model, but with a new path
                or os.path.getmtime(path) > self.models[name]['time'] + 300 # Existing model and path, but newer
                ):
                m = tc.load_model(path)
                self.models[name] = {'model': m,
                                     'time': os.path.getmtime(path),
                                     'users': m.get_num_users_per_item(),
                }
                self.model_paths[name] = path
        except FileNotFoundError:
            pass

    @_ensure
    def recommend(self, name, *args, **kwargs):
        k = None
        if 'k' in kwargs:
            # Get some extra recommendations because we might filter some out
            k = kwargs['k']
            kwargs['k'] = int(1.2*k)

        out = []
        recs = self.models[name]['model'].recommend(*args, **kwargs)
        for r in recs:
            if self.num_users(name, r['_id']) < 1000:
                continue
            else:
                out.append(r)

        if k:
            out = out[:k]

        return out

    @_ensure
    def predict(self, name, *args, **kwargs):
        return self.models[name]['model'].predict(*args, **kwargs)

    @_ensure
    def num_users(self, name, item, **kwargs):
        try:
            return self.models[name]['users'][self.models[name]['users']['_id'] == item]['num_users'][0]
        except:
            return 0

    @_ensure
    def compare_users(self, name, user1, user2):
        try:
            coefficients = self.models[name]['model'].coefficients['user']
            c_1 = coefficients[coefficients['user'] == user1][0]['factors']
            c_2 = coefficients[coefficients['user'] == user2][0]['factors']
            sim = dot(c_1, c_2)/(norm(c_1)*norm(c_2))
            return int(sim*100)/100.0
        except IndexError:
            return -1

engine = Recengine({"anime": "models/explicit_anime.model",
                    "manga": "models/explicit_manga.model"})

def get_user_recommendations(
        user,           # User for which to generate recommendations
        type,           # Type of recommendation (manga, anime, etc) (only anime supported as of now)
        genres,         # List of allowed genres
        all_genres,     # If true anime must include *all* allowed genres; otherwise, it only needs to include one
        start_year,     # All anime must start on this year or later
        end_year,       # All anime must end on this year or earlier
        staff,          # List of obligatory staff roles
        all_staff,      # If True, all the above staff roles must be present. Otherwise, only one needs to
        types,          # List of allowed types, e.g. 'OVA', 'TV', etc
        remove_sequels, # Remove sequels to shows
        plan_to_watch,  # Possibilities:
                        # 'Show' - show items on the user's PTW list
                        # 'Hide' - hide items on the user's PTW list
                        # 'Only' - only show items on the user's PTW list

        number=30,      # Number of recommendations to generate
):

    user_list = list(toshokan.lists.find({"user":user, "library_ref.$ref":type}, {'_id': 0, 'user': 0}))
    print('User list length: ' +str(len(user_list)))

    genre_query = '$all' if all_genres else '$in'
    query = {'genres': {genre_query: genres},
             'start_date': {'$gte': datetime.datetime(start_year, 1, 1),
                            '$lte': datetime.datetime(end_year, 12, 31)},
             'type': {'$in': types}}
    valid_anime = toshokan.anime.find(query,
                                      {'_id': 1})

    valid_anime = set([a['_id'] for a in valid_anime])

    total_elligible = []
    for pair in staff:
        role, entity = pair.split('.*.')
        if entity == "null" or role == "null":
            continue

        if role == 'Character':
            elligible_anime = set([x['anime']['$id'] for x in toshokan.anime_cast.find({'character.$id': entity}, {'anime.$id':1})])
        elif role == 'Studio':
            elligible_anime = set([x['_id'] for x in toshokan.anime.find({'studios': {'$in': [entity]}})])
        elif role == 'Voice Actor':
            elligible_anime = set([x['anime']['$id'] for x in toshokan.anime_cast.find({'voices.$id': entity}, {'anime.$id':1})])
        else:
            elligible_anime = set([x['anime']['$id'] for x in toshokan.anime_staff.find({'person.$id':entity, 'roles':{'$in':[role]}}, {'anime.$id':1})])

        if all_staff:
            valid_anime = valid_anime.intersection(elligible_anime)
        else:
            total_elligible += list(elligible_anime)

    valid_anime = list(valid_anime)

    if not all_staff:
        valid_anime = [x for x in valid_anime if x in total_elligible]

    user_planned = [x['library_ref'].id for x in user_list if x['status'] == 'planned']
    user_completed = set([x['library_ref'].id for x in user_list]) - set(user_planned)

    recs = [x["_id"] for x in engine.recommend(type, users=[user], items=valid_anime, k=200)]

    # This will happen if the user has watched anime since the last re-training
    recs = [r for r in recs if r not in user_completed]

    has_sequels = [x['_id'] for x in toshokan[type].find({'_id': {'$in': recs}}, {'links':1}) if 'Prequel' in x['links']]

    if remove_sequels:
        recs = [r for r in recs if r not in has_sequels]

    if plan_to_watch == 'Hide':
        recs = [r for r in recs if r not in user_planned]
    elif plan_to_watch == 'Only':
        recs = [r for r in recs if r in user_planned]

    recs = recs[:number]
    min_data = list(toshokan.anime.find({'_id': {'$in': recs}}, {'title':1, '_id': 1, 'episode_count': 1, 'type': 1, 'duration': 1, 'volume_count': 1, 'chapter_count': 1, 'genres':1, 'type': 1, 'duration': 1, 'start_date':1, 'end_date': 1, 'source': 1}))

    return {
        'recommendations': recs,
        'data': {
            obj["_id"]: { key: value for key, value in obj.items() if key != "_id"} for obj in min_data

        }
    }

def compare_user(user, username, type='anime'):
    user2 = toshokan.users.find_one({"username":username}, {"_id": 1, 'settings': 1})
    if not user2['settings']['list_public']:
        return {
            'similarity': -1
        }

    user2 = user2["_id"]
    sim = int(engine.compare_users(type, user, user2) * 100)
    return {
        'similarity': int(engine.compare_users(type, user, user2) * 100)
    }

def sakura_score(user, id, type='anime'):
    if engine.num_users(type, id) < 1000:
        score = -1
    else:
        score = int(engine.predict(
            type,
            tc.SFrame({'user':[user], "_id":[id]})
        )[0]*100)
        if score > 1000:
            score = 1000
        elif score < 0:
            score = 0

    return {'score': score}

def get_top(type):
    recs = [x["_id"] for x in engine.recommend(type, users=["undefined"], k=60)]
    min_data = list(toshokan.anime.find({'_id': {'$in': recs}}, {'title':1, '_id': 1, 'episode_count': 1, 'type': 1, 'duration': 1, 'volume_count': 1, 'chapter_count': 1, 'genres':1, 'type': 1, 'duration': 1, 'start_date':1, 'end_date': 1, 'source': 1}))

    return {
        "top": recs,
        'data': {
            obj["_id"]: { key: value for key, value in obj.items() if key != "_id"} for obj in min_data
        }
    }

def reload_all_models(user):
    engine.refresh()
