import json
import re
from datetime import datetime

from slugify import slugify, SLUG_OK
from bson.json_util import dumps

def library_id(title):
    title_clean = re.sub("""[☆★＊]+""", '*', title)
    title_clean = re.sub("""[×]+""", 'x', title_clean)
    title_clean = re.sub("""[␣]+""", '_', title_clean)
    title_clean = re.sub("""[†]+""", '+', title_clean)
    title_clean = re.sub("""[♭]+""", 'b', title_clean)
    title_clean = re.sub("""[%]+""", ' percent ', title_clean)
    title_clean = re.sub("""[&]+""", ' and ', title_clean)
    title_clean = title_clean[0]+re.sub("""[.]+""", ' ', title_clean[1:]) #only replace after first character
    title_clean = re.sub("""[—∀®^​→:'”=“\]ー\[(/◎≠,♡⤴￥\"↑♪;#∞!°>♂–’▲△?)♥・]+""", ' ', title_clean)
    title_clean = re.sub('[½]+', '1-2', title_clean)
    title_clean = slugify(title_clean, only_ascii=True, ok=SLUG_OK+"@$_+*.○◯")
    library_id = title_clean = re.sub("""[○◯]+""", 'O', title_clean)
    if not library_id:
        raise ValueError
    return library_id
