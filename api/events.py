from collections import defaultdict

from api.clients import toshokan
from api.library import min_projection
from bson import ObjectId
from bson.objectid import InvalidId
def get_user_events(user, ns="list", amount=None, after=None, before=None, social_feed=False, from_user=None, item_data=False):
    amount = (amount or 10) if not after else None
    query = {"ns":ns, "user":user}
    data = {}
    #social feed
    if social_feed:
        data['usernames'] = {}
        user_obj = toshokan.users.find_one({"_id":user})
        following = user_obj.get("following", [])
        if following:
            following_valid = [user for user in toshokan.users.find({"_id": {"$in": following}})
                                   if user["settings"]["list_public"]]
            query["user"] = {"$in": [user["_id"] for user in following_valid]}
            data['usernames'] = {user['_id']: user['username'] for user in following_valid}
        else:
            return {"user_events": [], "data": {}}

    if after or before:
        query["_id"] = {}

    if after:
        try:
            after = ObjectId(after)
        except InvalidId:
            return {"user_events":[]}

        query["_id"]["$gt"] = after

    if before:
        try:
            before = ObjectId(before)
        except InvalidId:
            return {"user_events":[]}

        query["_id"]["$lt"] = before

    projection = {"library_ref":1, "operation":1, "fields":1, "time":1, "user":1}
    recent_events = list(toshokan.events.find(query, projection).sort("_id", -1)[:amount])
    if ns == "list" and item_data:
        d = {}
        items = defaultdict(list)

        for e in recent_events:
            items[e['library_ref'].collection].append(e['library_ref'].id)

        for c, i in items.items():
            d[c] = list(toshokan[c].find({'_id': {'$in': i}}, min_projection))

        data['items'] = {key: {obj['_id']:obj for obj in d[key]} for key in d.keys()}

    return {"user_events":recent_events, "data":data}

def get_public_events(username, ns="list", amount=None, after=None):
    user_obj = toshokan.users.find_one({"username":username}, {"settings":1})
    if user_obj["settings"]["list_public"] and ns == "list":
        events = get_user_events(user_obj["_id"], "list", amount, after, item_data=True)
        return events

    elif not user_obj["settings"]["list_public"]:
        return {"msg": "List is not public"}, 403

def set_following(user, username, follow=True):
    user_obj = toshokan.users.find_one({"_id":user})
    following = set(user_obj.get("following", []))
    follow_user_obj = toshokan.users.find_one({"username":username})
    if not follow_user_obj:
        return {"msg": "User doesn't exist"}, 400
    if follow:
        following.add(follow_user_obj["_id"])
    else:
        following.discard(follow_user_obj["_id"])

    toshokan.users.update_one({"_id":user}, {"$set": {"following": list(following)}})
