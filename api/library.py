from collections import defaultdict
from api.clients import toshokan

def get_library_object(collection, _id, projection=None):
    base_projection = {"date_added":0, "last_modified":0}

    projection = projection if projection else base_projection
    try:
        obj = toshokan[collection].find_one({"_id": _id}, projection)
        if not obj:
            raise ValueError

        return obj

    except ValueError:
        return {"msg":"Not Found"}, 404

min_projection = {"_id":1,"title":1, "episode_count":1, "type":1, "duration":1, "volume_count":1, "chapter_count":1, "studios":1, "genres":1, "authors":1}
creature_min_projection = {"_id":1,"nick_name":1, "name_english":1, "name_japanese":1, "name":1, "given_name":1, "family_name":1}

def get_anime(id, min=False):
    projection = None if not min else min_projection
    return get_library_object("anime", id, projection)

def get_manga(id, min=False):
    projection = None if not min else min_projection
    return get_library_object("manga", id, projection)

def get_novel(id, min=False):
    projection = None if not min else min_projection
    return get_library_object("novel", id, projection)

def get_vn(id, min=False):
    # Note: "min" is currently ignored
    data = get_library_object("vn", id)
    data['type'] = 'VN'
    relations = toshokan['vn_relations'].find({'from':id}, {'date_added': 0})
    rel_obj = defaultdict(list)
    for rel in relations:
        rel_obj[rel['relation']].append({
            'href': '/vn/' + rel['to'],
            'title': rel['to_title'],
            'official': rel['official'],
        })

    data['links'] = dict(rel_obj)
    return data

def get_person(id, min=False):
    projection = None if not min else creature_min_projection
    return get_library_object("person", id, projection)

def get_character(id, min=False):
    projection = None if not min else creature_min_projection
    return get_library_object("character", id, projection)

def get_cast_staff(role_type, type, id):
    try:
        obj_list = list(toshokan["%s_%s" % (type, role_type)].find({"%s.$id"%type: id}, 
                                                                    {"_id":0, "date_added":0, "last_modified":0, type:0}) )
        if not obj_list:
            raise ValueError

        return {role_type: obj_list}

    except ValueError:
        return {"msg":"Not Found"}, 404

def get_anime_cast(id):
    return get_cast_staff("cast","anime", id)

def get_manga_cast(id):
    return get_cast_staff("cast", "manga", id)

def get_novel_cast(id):
    return get_cast_staff("cast", "novel", id)

def get_anime_staff(id):
    return get_cast_staff("staff", "anime", id)

def get_vn_cast(id):
    return get_cast_staff("cast", "vn", id)

def get_vn_staff(id):
    return get_cast_staff("staff", "vn", id)

def get_anime_episodes(id):
    episodes = list(toshokan.anime_episode.find({"anime.$id": id}, {"anime":0, "last_modified":0, "date_added":0}))
    return {"episodes": episodes}

def get_character_roles(type, id):
    try:
        obj_list = list(toshokan["%s_cast" % type].find({"character.$id":id}, {"_id":0, "date_added":0, "last_modified":0, "character":0 }) )
        if not obj_list:
            raise ValueError

        return {"roles": obj_list}

    except ValueError:
            return {"msg":"Not Found"}, 404

def get_person_roles(type, id, role):
    default_projection = {"_id":0, "date_added":0, "last_modified":0 }
    try:
        if role == "staff" and type == "anime":
            coll = "anime_staff"
            field = "person"
            projection = {**default_projection, "person":0}
        elif role == "voice" and type == "anime":
            coll = "anime_cast"
            field = "voices"
            projection = default_projection
        elif role == "author":
            coll = type
            field = "authors"
            projection = {"_id":1, "authors":1, "title":1, "synonyms":1}
        else:
            print("invalid types")
            raise ValueError

        obj_list = list(toshokan[coll].find({"%s.$id" %field: id}, projection ) )
        if not obj_list:
            raise ValueError

        return {"roles": obj_list}

    except ValueError:
            return {"msg":"Not Found"}, 404
