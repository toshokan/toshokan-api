FROM python:3.6

USER root

RUN apt-get update && \ 
    apt-get -y install wget libblas3 liblapack3 libstdc++6 python-setuptools && \
    apt-get clean 

RUN pip install -U h5py keras
RUN pip install -I turicreate==5.1

COPY requirements.txt /
RUN pip install -r requirements.txt

COPY api /code/api
COPY framework /code/framework
COPY openapi /code/openapi
COPY api_endpoint.py /code/api_endpoint.py

EXPOSE 8128
