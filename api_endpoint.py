from gevent import monkey
monkey.patch_all()

import connexion

from framework import validator_map
import framework.format_validators # adds format validators to schema on import automatically
from framework.json_encoder import ToshokanAPIJSONEncoder

app = connexion.App(__name__, specification_dir='openapi/',validator_map=validator_map)
app.app.json_encoder = ToshokanAPIJSONEncoder
app.add_api('specification.yaml', strict_validation=True)

if __name__ == '__main__':
    app.run(port=8128, server="gevent")
