from jsonschema import draft4_format_checker
import iso8601

@draft4_format_checker.checks('date-time')
def is_datetime(val):
    if val == None:
        return True
    if not isinstance(val, str):
        return False
    try:
        iso8601.parse_date(val)
    except iso8601.ParseError:
        return False
    return True