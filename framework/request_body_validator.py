import jsonschema
import iso8601
from connexion.decorators.validation import RequestBodyValidator

# python-jsonschema
def extend_validator(validator_class):
    validate_properties = validator_class.VALIDATORS['properties']

    def set_properties(validator, properties, instance, schema):

        # run regular validation first
        for error in validate_properties(
                validator, properties, instance, schema):
            yield error

        for property, subschema in properties.items():
            # apply default values
            if 'default' in subschema:
                instance.setdefault(property, subschema['default'])
            # caste date strings to python datetime objects
            if 'format' in subschema and subschema['format'] == 'date-time' and instance[property]:
                instance[property] = iso8601.parse_date(instance[property])

        

    return jsonschema.validators.extend(validator_class, {'properties': set_properties})

ToshokanAPIDraft4Validator = extend_validator(jsonschema.Draft4Validator)

class ToshokanAPIRequestBodyValidator(RequestBodyValidator):
    def __init__(self, *args, **kwargs):
        super(ToshokanAPIRequestBodyValidator, self).__init__(
            *args, validator=ToshokanAPIDraft4Validator, **kwargs)