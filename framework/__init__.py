from .request_body_validator import ToshokanAPIRequestBodyValidator

validator_map = {
    'body': ToshokanAPIRequestBodyValidator
}
