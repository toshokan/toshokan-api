from bson import DBRef, ObjectId, Timestamp
from connexion.apps.flask_app import FlaskJSONEncoder

class ToshokanAPIJSONEncoder(FlaskJSONEncoder):
    def default(self, o):
        if isinstance(o, DBRef):
            return {"href": "/%s/%s" % (o.collection, o.id), "type": o.collection, **o._DBRef__kwargs}
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, Timestamp):
            return o.as_datetime()
        return super(ToshokanAPIJSONEncoder, self).default(o)